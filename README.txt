-- SUMMARY --

This module simply modify the theme registry & locale settings to force  Garland Theme , LTR & English to be set when UserID = 1 (admin)


-- REQUIREMENTS --
Locale module


-- INSTALLATION --

* Install as usual & enable 


-- CONFIGURATION --
for now there is no config and the default for admin is LTR & english, patches are welcome!

-- CUSTOMIZATION --

none, but ill be happy if someone can help to fix admin_theme bug and do some configuration & customize screen to choose theme instad only for garland

-- CONTACT  & CREDITS --

Made by Amir Ashkenazi
Skype: amircx
gtalk : amircx2@gmail.com
email: amir@amir.cx


--- other -------------

this is a very basic module that does the job without hundereds lines of code, i tried to keep it simple and short as possible

Similar modules:
Administration Language (http://drupal.org/project/admin_language)











--- In Memorial -------------
This module is made in memory of my brother, tal ashkenazi, 
who was programmer too and died from Lung Disease (cystic fibrosis - CF)
"Don’t take your organs to heaven, God knows we need them here" - http://www.donatelifetoday.com/


